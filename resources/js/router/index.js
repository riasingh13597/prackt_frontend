import { createWebHistory, createRouter } from 'vue-router'


import App from '../App.vue'
import Main from '../Main.vue'
const routes = [
    {
        name: "search",
        path: "/search",
        component: Main,
       
    },
    {
        path: '/',
        name: 'home',
        component: App
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes, // short for `routes: routes`
})

export default router
